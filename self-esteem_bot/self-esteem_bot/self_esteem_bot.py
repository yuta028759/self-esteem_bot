import tweepy,datetime,config as cf
 
#認証
auth = tweepy.OAuthHandler(cf.consumer_key, cf.consumer_secret)
auth.set_access_token(cf.access_token, cf.access_token_secret)
 
api = tweepy.API(auth)

now=datetime.datetime.today().strftime("%Y/%m/%d %H:%M:%S")

api.update_status(now+' テスト')
